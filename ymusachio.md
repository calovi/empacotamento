 - Qual seu nome?
Yuri Musachio Montezuma da Cruz

- Qual sua idade?
29

- Qual sua área de formação? (não necessarimente acadêmica):
Redes de Computadores

- Qual sua área de atuação?
Redes de Computadores e Sistemas Operacionais

- A Quanto tempo usa GNU/Linux?
Aproximadamente 2 anos

- Adminstra ou administrou servidores GNU/Linux?
Sim

- A quanto tempo usa Debian?
Aproximadamente 2 anos

- Qual sua distribuição preferida?
Debian

- Em seu computador pessoal qual sistema e distribuição principal?
Não tenho um computador pessoal, então acabo usando o do trabalho, que por minha decisão só há o Debian com OS.

- Qual seu editor de texto puro preferido?
VIM

- É programador(a)? Que linguagens?
Em 2014, na época em que fazia Sistemas de Informação, cheguei a aprender C. Apesar de ter gostado, não dei continuidade no aprendizado da linguagem... Os professores me desanimaram em aprender a desenvolver essa linguagem ou qualquer outra. Hoje em dia lembro pouca coisa, e por isso não desenvolvo.

- Tem experiência em compilar programas?
Não

- Já criou pacotes deb?
Não

- Já criou pacotes de alguma outra distribuição?
Não
