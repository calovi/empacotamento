## Tico

Olá, Kretcheu! Seguem minhas respostas:

Qual seu nome?
Thiago Pezzo (tico)

Qual sua idade?
44 anos.

Qual sua área de formação? (não necessarimente acadêmica):
Sociologia.

Qual sua área de atuação?
Assistência Social, Educação e Pesquisa.

A Quanto tempo usa GNU/Linux?
11 anos.

Adminstra ou administrou servidores GNU/Linux?
Não.

A quanto tempo usa Debian?
3 anos.

Qual sua distribuição preferida?
GNU/Linux Debian

Em seu computador pessoal qual sistema e distribuição principal?
GNU/Linux Debian Bullseye (estável)

Qual seu editor de texto puro preferido?
Vi/Vim

É programador(a)? Que linguagens?
Como hobby em nível básico; noções de shell, Python, Javascript/HTML/CSS

Tem experiência em compilar programas?
Não.

Já criou pacotes deb?
Não.

Já criou pacotes de alguma outra distribuição?
Não.


Abraços!
Thiago


