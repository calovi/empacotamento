## Links para documentação

- Criando chave gpg
https://keyring.debian.org/creating-key.html
https://www.digitalneanderthal.com/post/gpg/

- Debian New Maintainers' Guide
https://www.debian.org/doc/manuals/maint-guide/

- Mentors
https://mentors.debian.net/intro-maintainers/

- Relatar BUGs
https://www.debian.org/Bugs/Reporting

- Sistema de bugs BTS
https://www.debian.org/Bugs/server-control

- Links do João Eriberto
https://people.debian.org/~eriberto/#links
http://eriberto.pro.br/wiki/index.php?title=FAQ_empacotamento_Debian

Vídeo sobre número de versões:
https://youtu.be/SEJs_VmdqtA

Slides:
http://eriberto.pro.br/files/videoaulas/debian/empacotamento_mini_aula_formatos_numeros_versao.pdf

- Debian Policy Manual
https://www.debian.org/doc/debian-policy/

- Referência para desenvolvedores
https://www.debian.org/doc/manuals/developers-reference/developers-reference.en.pdf
https://www.debian.org/doc/manuals/developers-reference/index.en.html

- Guia dos mantenedores
https://www.debian.org/doc/manuals/maint-guide/maint-guide.en.pdf

- Como assinar releases no GithUb
https://wiki.debian.org/Creating%20signed%20GitHub%20releases

- Licenças
https://www.debian.org/legal/licenses/

- Manual de empacotamento
https://www.debian.org/doc/manuals/packaging-tutorial/packaging-tutorial.pt.pdf

- Lista de discussão
https://lists.debian.org/

- Sobre watch
https://wiki.debian.org/debian/watch

- Outras formas de empacotar
https://hashman.ca/debian-build-tools/

- Convertendo debian.diff
https://wiki.debian.org/Projects/DebSrc3.0

## Links do curso

- Tmate
https://tmate.io/t/kretcheu/debpkg
ssh kretcheu/debpkg@nyc1.tmate.io

- Aulas
https://jitsi.malamanhado.com.br/debpkg


