# Curso de empacotamento Debian

Crie um arquivo seu-nick.md respondendo as seguintes perguntas:

## Questionário para os alunos(as)

- Qual seu nome?
- Qual sua idade?
- Qual sua área de formação? (não necessarimente acadêmica):

- Qual sua área de atuação?

- A Quanto tempo usa GNU/Linux?

- Adminstra ou administrou servidores GNU/Linux?

- A quanto tempo usa Debian?

- Qual sua distribuição preferida?

- Em seu computador pessoal qual sistema e distribuição principal?

- Qual seu editor de texto puro preferido?

- É programador(a)? Que linguagens?

- Tem experiência em compilar programas?

- Já criou pacotes deb?

- Já criou pacotes de alguma outra distribuição?

