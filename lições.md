## Lições

- 0 - Jaula SID
- 1 - baixar e construir pcapfix
- 2 - baixar e construir nethogs
- 3 - debianizar pcapfix 1.1.4
- 4 - atualizar pcapfix
- 5 - Cadastro no mentors e envio da chave
- 6 - Instalar e testar cowbuilder
- 7 - Encontrar um pacote
- 8 - Debianizar e testar no cowbuilder
- 9 - Fazer um QA
- 10- Simular/fazer um ITP
- 11- Preparar um ITA
- 12- Fazer um RFS
- 13- Criar um manual
