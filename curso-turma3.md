# Curso Empacotamento Debian (40h)
# Fevereiro 2022

## Objetivos

Ser capaz…
- de construir um pacote existente a partir dos fontes
- de fazer alterações em pacotes existentes
- de criar um pacote novo

Dinâmica:
- conceitos
- laboratórios
- atividades individuais
- oficinas
- material de apoio

## Conteúdo

- Fluxo dos pacotes no Debian
- Bugs RFP, ITP, RFS
- Anatomia de um pacote deb
- Métodos de empacotamento
- Preparação da jaula
- Pacotes fonte
- Diretório debian
- Ferramentas de desenvolvimento
- Construção de pacotes
- Atualização de pacotes
- Debianização
- Debian tracker
- Wnpp-check
- Debian policies
- Mais estudos (gbp, sbuild)

## Atividades

- Serão 8 aulas de 1h30 aproximadamente disponibilizadas aos alunos.
- 8 Laboratórios/tira-dúvidas ao vivo via jitsi quarta e sábado.

## Material

- Serão disponibilizados os vídeos para participantes.
- Material de apoio composto de slides, guias e links.

## Datas propostas

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
2   | qua | 20h    |  | ***
5   | sáb | 14h    |  | ***
9   | qua | 20h    |  | ***
12  | sáb | 14h    |  | ***
16  | qua | 20h    |  | ***
19  | sáb | 14h    |  | ***
23  | qua | 20h    |  | ***
25  | sáb | 14h    |  | ***

-------
## calendário

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
.    |  .   |  [2] | . |  .  |  [[5]]
.    |  .   |  [9] | . |  .  | [[12]]
.    |  .   | [16] | . |  .  | [[19]]
.    |  .   | [23] | . |  .  | [[26]]

-------

```
[aula]
[[Laboratório]]
```

## Custo e condições

- R$ 200,00 via pix ou parcelado no cartão via pagseguro.

Pix:
kretcheu@gmail.com

PagSeguro
https://pag.ae/7XW9poQ13

## Contato

- e-mail: kretcheu@gmail.com
- Telegram: @kretcheu
