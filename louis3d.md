# Curso de empacotamento Debian

Crie um arquivo seu-nick.md respondendo as seguintes perguntas:

## Questionário para os alunos(as)

- Qual seu nome?  
    Luis Felipe Leal
- Qual sua idade?  
    25
- Qual sua área de formação? (não necessarimente acadêmica):  
    Ciência da Computação
- Qual sua área de atuação?  
    Desenvolvimento
- A Quanto tempo usa GNU/Linux?  
    5 anos
- Adminstra ou administrou servidores GNU/Linux?  
    Sim
- A quanto tempo usa Debian?  
    2-3 anos
- Qual sua distribuição preferida?  
    Debian
- Em seu computador pessoal qual sistema e distribuição principal?  
    Debian
- Qual seu editor de texto puro preferido?  
    VIM
- É programador(a)? Que linguagens?  
    Java,PHP,Python
- Tem experiência em compilar programas?  
    Não
- Já criou pacotes deb?  
    Não
- Já criou pacotes de alguma outra distribuição?  
    Não
