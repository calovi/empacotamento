# Curso de empacotamento Debian

## Question�rio para os alunos(as)

- Qual seu nome?

Marcelo Barboza.

- Qual sua idade?

Tenho 36 anos.

- Qual sua �rea de forma��o? (n�o necessarimente acad�mica):

Sou bacharel em matem�tica.

- Qual sua �rea de atua��o?

Atuo como professor de matem�tica.

- A Quanto tempo usa GNU/Linux?

Desde de agosto de 2008. Comecei com o Ubuntu, passando em seguida para o
Debian.

- Administra ou administrou servidores GNU/Linux?

Sim, como hobby. No meu trabalho n�o tenho, obviamente, a necessidade de manter
eu mesmo um servidor de computador. As m�quinas que mantive forneciam servi�os
para meus familiares, exclusivamente. Atualmente, mantenho apenas um servidor
na nuvem.

- A quanto tempo usa Debian?

Desde 2011.

- Qual sua distribui��o preferida?

Eu j� utilizei v�rias distribui��es. Dentre elas, as seguintes:

- Ubuntu;
- Debian;
- Mint;
- Fedora;
- CentOS;
- Arch;
- Manjaro;
- Slackware.

Mas as minhas preferidas s�o, sem d�vida alguma, Debian e Slackware. Devo dizer
que tenho muita vontade de conhecer [NixOS](https://nixos.org).

- Em seu computador pessoal qual sistema e distribui��o principal?

Utilizo o Debian GNU/Linux 11 (bullseye).

- Qual seu editor de texto puro preferido?

[Vim](https://www.vim.org) sem a menor d�vida alguma. Utilizo tamb�m, embora
com menor frequ�ncia, os seguintes editores (nesta ordem):

- [sed](https://www.gnu.org/software/sed/manual/sed.html);
- [ed](https://gnu.org/software/ed);
- [nvi](https://packages.debian.org/stable/editors/nvi);
- [neovim](https://neovim.io).

- � programador(a)? Que linguagens?

Sim, embora me considere um eterno aprendiz. Entendo bem a linguagem Python,
estou a aprender a linguagem C. Comecei com Pascal (na faculdade). Utilizei por
muitos anos uma linguagem chamada [GAP](https://gap-system.org).

- Tem experi�ncia em compilar programas?

Bem, al�m dos programas que fa�o em C para o meu uso pessoal (isso conta?), eu
quase sempre compilei a linguagem GAP a partir do fonte como forma de adaptar a
instala��o para as minhas necessidades.

- J� criou pacotes deb?

N�o, nunca.

- J� criou pacotes de alguma outra distribui��o?

N�o, nunca.
